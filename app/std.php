<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class std extends Model
{
    protected $fillable = [
        'stdName', 'facilities', 'phone',
    ];
}
