<?php

namespace App\Http\Controllers;

use App\std;
use Illuminate\Http\Request;


class StdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stds = std::all();
        return view('stds.index',compact('stds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('stds.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'stdName'=> 'required|max:15',
            'facilities'=> 'required|max:50',
            'phone'=> 'required|min:9|max:9'
        ]);

        // $stds = new std;
        // $stds->stdName = $request->input('stdName');
        // $stds->facilities = $request->input('facilities');
        // $stds->phone = $request->input('phone');
        // $stds->save();
        $stds = new std([
            'stdName' => $request->input('stdName'),
            'facilities' => $request->input('facilities'),
            'phone' => $request->get('phone'),   
        ]);

        $stds->save();

        return redirect('/stds')->with('success', 'Студент успешно добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\std  $std
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $std = std::find($id);

        return view('stds.show', compact('std'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\std  $std
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $std =std::find($id);
        return view('stds.edit',compact('std'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\std  $std
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'stdName'=> 'required|max:30',
            'facilities'=> 'required|max:30',
            'phone'=> 'required|max:9|min:9'
        ]);

        $std = std::find($id);
        $std->stdName = $request->get('stdName');
        $std->facilities = $request->get('facilities');
        $std->phone = $request->get('phone');
        $std->save();

        return redirect('/stds')->with('success', 'Анкета успешно отредактирована!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\std  $std 
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $std=std::find($id);
        $std->delete();
        return redirect('/stds')->with('success','Студент успешно удален!');
    }

}
