@extends('layouts.app')

@section('stdName', 'Редактировать Анкету '.$std->stdName)

@section('content')
<div class="row">
<div class="col-lg-6 mx-auto">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif

    <form method="POST" action="{{ route('stds.update', $std) }}">
     @csrf
     @method('PATCH') 
     <div class="form-group">
            <label for="std-stdName" class="form-label">Имя Студента</label>
            <input type="text" value="{{ $std->stdName }}" name="stdName" class="form-control" id="std-stdName" >
        
        </div>

        <div class="form-group">
            <label for="std-facilities" class="form-label">Факультет</label>
            <input type="text" value="{{ $std->facilities }}" name ="facilities" class="form-control" id="std-facilities">
        </div>
        
        <div class="form-group">
            <label for="std-phone" class="form-label">Номера</label>
         

            <input type="text" name="phone" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" type="number" id="std-phone" name="phone" class="form-control" placeholder="123-456-678" required value="{{ $std->phone }}">
        </div>
        <button type="submit" class="btn btn-success">Отредактировать</button>
    </form>
</div>
</div>
@endsection