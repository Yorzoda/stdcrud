@extends('layouts.app')

@section('title', $std->stdName)

@section('content')
<div class="card">
    <div class="card-body">
       <h3>{{ $std->stdName }}</h3>
       <p>{{ $std->facilities }}</p>
       <p><b>{{ $std->phone }}</b></p>
    </div>
</div>
@endsection