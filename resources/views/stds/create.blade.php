@extends('layouts.app')

@section('title', 'Добавить Студента')

@section('content')
<div class="row">
    <div class="col-lg-5 mx-auto">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        <form method="POST" action="{{ route('stds.store') }}">
        @csrf
        <div class="form-group">
            <label for="std-stdName" class="form-label">Имя Студента</label>
            <input type="text" name="stdName" class="form-control" id="std-stdName" >
        
        </div>

        <div class="form-group">
            <label for="std-facilities" class="form-label">Факультет</label>
            <input type="text" name ="facilities" class="form-control" id="std-facilities">
        </div>
        
        <div class="form-group">
            <label for="std-phone" class="form-label">Номера</label>
         

            <input type="text" name="phone" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" type="number" id="std-phone" name="phone" class="form-control" placeholder="123-456-678" required>
        </div>

        <button type="submit" class="btn btn-success">Создать анкету</button>

        </form>
    </div>
</div>
@endsection

