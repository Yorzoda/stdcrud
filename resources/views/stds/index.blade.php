@extends('layouts.app')

@section('title', 'Page Title')

@section('content')
<a href="{{ route('stds.create') }}" class="btn btn-success">Добавить студента</a>


<table class="table table-striped mt-2">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Имя</th>
      <th scope="col">Факультет</th>
      <th scope="col">Телефон</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
  @foreach($stds as $std) 
    <tr>

      <th scope="row">{{ $std->id }}</th>
      <td>{{ $std->stdName }}</td>
      <td>{{ $std->facilities }}</td>
      <td>{{ $std->phone }}</td>
      <td class="table-buttons">
        <a href="{{ route('stds.show',$std) }}" class="btn btn-success">
            <i class="fa fa-eye" ></i>
        </a>
        <a href="{{ route('stds.edit',$std) }}" class="btn btn-primary">
            <i class="fa fa-pencil" ></i>
        </a>
        <form method="Post" action="{{ route('stds.destroy', $std) }}"">
        @csrf
        @method('DELETE')
        <button type ="submit" class="btn btn-danger" >
            <i class="fa fa-trash"></i>
        </button>
        </form>
      </td>

    </tr>
    @endforeach
  </tbody>
</table>
@endsection

